const fs = require("fs");
const fg = require("fast-glob");
const pdf = require("pdf-parse");
const { promisify } = require("util");

const readF = promisify(fs.readFile);

const pdfParsedToTotal = (data) => {
  const totalRegexp = /\n(.*)\nSubtotal: \$\n/g;
  const totalStr = totalRegexp.exec(data.text)[1];
  if (!totalStr) {
    console.warn("Sin total");
    return 0;
  }
  return parseFloat(totalStr.replace(",", "."));
};

const main = async () => {
  const files = await fg("files/*.pdf");
  const totals = [];
  for (const file of files) {
    const readFile = await readF(file);
    totals.push({
      file,
      total: pdfParsedToTotal(await pdf(readFile, { max: 1 })),
    });
  }
  console.table(totals);
  console.log(
    "----------- TOTAL ---------- \n",
    totals.reduce((a, b) => a + b.total, 0)
  );
};

main();
