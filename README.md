# SumaFacturasAfip

Script **muy rudimentario** que devuelve total facturado a partir de un set de facturas pdf de afip.

Lo desarrollé en un ratito como alternativa a tomar nota del total de cada factura una por una y sumarlas (como monotributista quería sumar un set de facturas C que tenía guardadas como pdf).

## Cómo usarlo

- clonar este repositorio `git clone https://gitlab.com/MatiasArriola/suma-facturas-afip.git`
- ubicar las facturas a calcular en el directorio `files`
- `npm run start`
